cmake_minimum_required(VERSION 3.0)
project(cecinput)

find_package(ECM CONFIG REQUIRED)
set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake" ${ECM_MODULE_PATH})

find_package(KF5Wayland CONFIG REQUIRED)

include(KDEInstallDirs)
include(KDECompilerSettings NO_POLICY_SCOPE)
include(KDECMakeSettings)
include(ECMQtDeclareLoggingCategory)
include(FindPkgConfig)
include(FeatureSummary)

pkg_check_modules(LibCEC IMPORTED_TARGET libcec)
add_feature_info(LibCEC LibCEC_FOUND "Required for CEC support (remote control over HDMI support)")

ecm_qt_declare_logging_category(ceclogging_SOURCES
    HEADER
        cec_logging.h
    IDENTIFIER
        KWIN_CEC
    CATEGORY_NAME
        kwin_cec
    DEFAULT_SEVERITY
        Debug
)

add_executable(cecinput src/main.cpp src/cecinput.cpp ${ceclogging_SOURCES})

target_link_libraries(cecinput KF5::WaylandClient PkgConfig::LibCEC)

configure_file(org.kde.cecinput.desktop.cmake ${CMAKE_CURRENT_BINARY_DIR}/org.kde.cecinput.desktop @ONLY)

install(TARGETS cecinput DESTINATION ${KDE_INSTALL_TARGETS_DEFAULT_ARGS})
install(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/org.kde.cecinput.desktop DESTINATION ${KDE_INSTALL_APPDIR} )
