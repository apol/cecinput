#ifndef CECINPUT_H
#define CECINPUT_H

#include <QObject>

namespace CEC {
    class ICECAdapter;
}

namespace KWayland {
    namespace Client {
        class FakeInput;
    }
}

class CECInput : public QObject
{
    Q_OBJECT
public:
    CECInput();
    ~CECInput() override;

    KWayland::Client::FakeInput* m_waylandInput = nullptr;

private:
    bool m_opened = false;
    CEC::ICECAdapter *m_cecAdapter = nullptr;
};

#endif // CECINPUT_H
